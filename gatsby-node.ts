import { GatsbyNode } from "gatsby"
import { createPosts, createStaticPages, createIndices, createCheetsheets } from "./src/generators";

export const createPages: GatsbyNode["createPages"] = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions
  await createPosts({ createPage, graphql, reporter })
  await createStaticPages({ createPage, graphql, reporter })
  await createIndices({ createPage, graphql, reporter })
  await createCheetsheets({ createPage, graphql, reporter })
}

// export const createSchemaCustomization: GatsbyNode["createSchemaCustomization"] = ({ actions }) => {
//   const { createTypes } = actions
//   const typeDefs = `
//     type BlogPost implements Node {
//       promoted: Boolean
//     }
//   `
//
//   createTypes(typeDefs)
// }
