import * as React from "react";
import {GatsbyBrowser, GatsbySSR} from "gatsby"
import {Layout} from "./src/layout";

// Add elements to the <head>, ie. <script /> tags
const HeadComponents: React.ReactNode[] = []

// Add classes to the <body> tag
const BodyAttributes = {
  className: "text-gray-700",
}

export const onRenderBody: GatsbySSR["onRenderBody"] = ({ setBodyAttributes, setHeadComponents }, pluginOptions) => {
  setBodyAttributes(BodyAttributes);
  setHeadComponents(HeadComponents);
}

// Wraps every page in a component
export const wrapPageElement: GatsbySSR["wrapPageElement"] = ({ element, props }) => {
  return (
      <Layout {...props}>
        {element}
      </Layout>
  )
}
