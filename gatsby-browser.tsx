import * as React from "react";
import { GatsbyBrowser } from "gatsby"
import { Layout } from "./src/layout"

import "./src/styles/index.css"

// Wraps every page in a component
export const wrapPageElement: GatsbyBrowser["wrapPageElement"] = ({ element, props }) => {
  return (
    <Layout {...props}>
      {element}
    </Layout>
  )
}
