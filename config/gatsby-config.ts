import * as path from "path"
import * as dotenv from "dotenv"

// Slurp env vars from .env file in root of repo
const envPath = path.join(__dirname, '..', '..', '.env')
dotenv.config({ path: envPath })

import plugins from "./plugins/plugins"
import sources from "./plugins/sources"
import devSources from "./plugins/sources-dev"
import prodPlugins from "./plugins/plugins-prod"

const inProduction = process.env.PRODUCTION === "true"
const inDevelopment = !inProduction // TODO:

function requireEnv(envKey: string) {
  if (process.env[envKey] === undefined) {
    throw new Error(`Required env var '${envKey}' is missing - Is your .env file present in the current directory?`)
  }
}
// if (inProduction) {
//   ['GATSBY_ALGOLIA_APP_ID', 'GATSBY_ALGOLIA_ADMIN_KEY', 'GATSBY_ALGOLIA_SEARCH_KEY'].forEach(requireEnv)
// }

export default {
  siteMetadata: {
    title: "absolutejam.co.uk",
    siteUrl: "https://absolutejam.co.uk",
    description: "My blog",
  },
  plugins: [
    ...plugins,

    // Sources
    ...sources,
    ...(inProduction ? prodPlugins() : []),
    ...(inDevelopment ? devSources() : []),
  ],
};
