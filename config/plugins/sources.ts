import { PluginConfig } from "./types"

import * as path from "path"
const root = path.resolve(__dirname, "../..")

const sources : (PluginConfig | string)[] =  [
  {
    resolve: "gatsby-source-filesystem",
    __key: "pages",
    options: {
      name: "pages",
      path: `${root}/content/pages`,
    },
  },
  {
    resolve: "gatsby-source-filesystem",
    __key: "posts",
    options: {
      name: "posts",
      path: `${root}/content/posts`,
    },
  },
  {
    resolve: "gatsby-source-filesystem",
    __key: "cheatsheets",
    options: {
      name: "cheatsheets",
      path: `${root}/content/cheatsheets`,
    },
  },
  {
    resolve: "gatsby-source-filesystem",
    __key: "fragments",
    options: {
      name: "fragments",
      path: `${root}/content/fragments`,
    },
  },
]


export default sources
