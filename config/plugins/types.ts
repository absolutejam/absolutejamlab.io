export interface PluginConfig {
  resolve: string;
  options?: any;
  __key?: string;
}