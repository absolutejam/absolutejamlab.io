export default () => [
  {
    resolve: "gatsby-source-filesystem",
    options: {
      name: "unpublished",
      path: "./content/unpublished/",
    },
    __key: "images",
  },
]
