import * as path from "path"
import { PluginConfig } from "./types"

const plugins : (PluginConfig | string)[] = [
  "gatsby-plugin-sass",
  {
    resolve: "gatsby-plugin-react-svg",
    options: {
      rule: {
        include: /svg/,
      },
    },
  },
  "gatsby-plugin-image",
  "gatsby-plugin-mdx",
  "gatsby-plugin-sharp",
  "gatsby-transformer-sharp",
  "gatsby-plugin-postcss",
  {
    resolve: "gatsby-plugin-alias-imports",
    options: {
      alias: {
        "@types/*": path.resolve(__dirname, "../../src/@types"),
      },
    },
  },
]

export default plugins
