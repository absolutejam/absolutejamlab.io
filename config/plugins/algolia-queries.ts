import type { PostFrontmatter } from "post"

const indexName = `absolutejam-blog` // TODO: Config

const postsQuery = `
  query postsQuery {
    allMdx {
      nodes {
        id
        frontmatter {
          slug
          title
        }
        excerpt(pruneLength: 5000)
      }
    }
  }
`
export interface QueryResultPostNode {
  id: string
  frontmatter: PostFrontmatter
  excerpt: string
}

function pageToAlgoliaRecord({ id, frontmatter, ...rest }: QueryResultPostNode) {
  return {
    objectID: id,
    ...frontmatter,
    ...rest,
  }
}

export const algoliaQueries = [
  {
    query: postsQuery,
    transformer: ({
      data,
    }: {
      data: { allMdx: { nodes: QueryResultPostNode[] } }
    }) => {
      return data.allMdx.nodes.map(pageToAlgoliaRecord)
    },
    indexName,
    settings: { attributesToSnippet: [`excerpt:20`] },
  },
]
