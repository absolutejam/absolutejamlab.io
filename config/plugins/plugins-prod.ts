import { algoliaQueries } from "./algolia-queries"

export default () => [
  // TODO: Currently unused!
  // {
  //   resolve: `gatsby-plugin-algolia`,
  //   options: {
  //     appId: process.env.GATSBY_ALGOLIA_APP_ID,
  //     apiKey: process.env.GATSBY_ALGOLIA_ADMIN_KEY,
  //     queries: algoliaQueries,
  //   },
  // },
]
