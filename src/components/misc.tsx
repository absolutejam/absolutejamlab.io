import * as React from "react"
import {ReactElement} from "react";

type SvgWrapperProps = { width: number, height: number, children: ReactElement } & React.HtmlHTMLAttributes<any>
export function SvgWrapper({ width,  height,  children, className }: SvgWrapperProps) {
    return (
        <div
            style={{width, height}}
            className={`flex items-center fill-current justify-center ${className}`}
        >
            {children}
        </div>
    )
}
