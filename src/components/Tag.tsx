import {Link} from "gatsby";
import React from "react";

export const Tag = ({ tag }: { tag: string }) => {
    return (
        <Link to={`/tags#tag-${tag}`}>
            <button className="flex text-gray-400 bg-gray-100 hover:bg-gray-200 text-sm rounded-lg px-3 py-1 focus:outline-none">
                {tag}
            </button>
        </Link>
    )
}
