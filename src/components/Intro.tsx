import * as React from "react"
import { MDXRenderer } from "gatsby-plugin-mdx"

export const Intro = ({ introBody }: { introBody: string }) => {
    return (
        <div className="flex flex-col p-6">
            <h1 className="text-4xl font-bold py-3">Welcome!</h1>
            <p className="py-2">
                <div className="markdown">
                    <MDXRenderer>{introBody}</MDXRenderer>
                </div>
            </p>
        </div>
    )
}
