import * as React from "react"
import { graphql } from "gatsby"
import { CheatsheetData } from "cheatsheet"
import { MDXRenderer } from "gatsby-plugin-mdx"

export const pageQuery = graphql`
    query recipeByName($name: String!) {
        mdx(frontmatter: { name: { eq: $name } }) {
            frontmatter {
                name
                title
            }
            body
        }
    }
`
interface Props {
    data: { mdx: CheatsheetData }
}

export default function RecipePage({ data: { mdx: post } }: Props) {
    const { frontmatter, body } = post
    const { title } = frontmatter
    return (
        <div className="flex flex-col p-6">
            <h1 className="text-4xl py-3 font-bold">{title}</h1>
            <div className="py-10 markdown">
                <MDXRenderer>{body}</MDXRenderer>
            </div>
        </div>
    )
}
