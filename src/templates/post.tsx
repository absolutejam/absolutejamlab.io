import React from "react"
import {graphql} from "gatsby"
import {PostData} from "post";
import {MDXRenderer} from "gatsby-plugin-mdx";
import {Tag} from "../components";

export const pageQuery = graphql`
    query postBySlug($slug: String!) {
        mdx(
            frontmatter: { kind: { eq: "Post" }
            slug: { eq: $slug } }
        ) {
            frontmatter {
                title
                slug
                published(formatString: "MMMM DD, YYYY")
                tags
            }
            body
        }
    }
`

interface Props {
    data: { mdx: PostData }
}

function PostSummary({date, tags}: { date: Date; tags: string[] }) {
    return (
        <div className="flex justify-center md:p-10 space-x-6">
            <div
                className="flex flex-col space-y-4 md:space-y-0 md:space-x-2 md:flex-row flex-1 border border-gray-200 rounded-lg justify-between items-center px-8 py-6 shadow-card transform hover:scale-102 transition ease-in">
                <div className="flex">Published {date.toString()}</div>
                <div className="flex flex-wrap justify-center md:justify-end space-x-2">
                    {tags.map((tag) => (
                        <Tag key={tag} tag={tag}/>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default function Post({data: {mdx: post}}: Props) {
    const {frontmatter: {title, published, tags}, body} = post
    return (
        <div className="flex flex-col p-6">
            <h1 className="text-4xl py-3 font-bold">{title}</h1>
            <div className="py-10 markdown">
                <MDXRenderer>{body}</MDXRenderer>
            </div>

            <PostSummary date={published} tags={tags || []}/>
        </div>
    )
}
