import React from "react"
import {PostData} from "post";
import {Link, graphql} from "gatsby"
import {SvgWrapper, Tag} from "../components"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const pageQuery = graphql`
    query indices($skip: Int!, $limit: Int!) {
        allMdx(
            sort: { fields: [frontmatter___published], order: DESC }
            filter: { frontmatter: { kind: { eq: "Post" } } }
            limit: $limit
            skip: $skip
        ) {
            nodes {
                frontmatter {
                    slug
                    title
                    tags
                    published(formatString: "MMMM DD, YYYY")
                }
            }
        }
    }
`

interface Props {
    pageContext: {
        currentPage: number
        numPages: number
    }
    data: {
        allMdx: {
            nodes: PostData[]
        }
    }
}

interface PostSummaryProps {
    title: string
    link: string
    published: Date
    tags?: string[]
}

export function PostSummary({title, link, published, tags}: PostSummaryProps) {
    return (
        <div
            className="flex flex-col space-y-1 p-8 border-gray-200 border rounded-lg transform hover:scale-102 transition ease-in focus:outline-none shadow-card">
            <div className="flex font-light text-gray-400 space-x-2 tracking-wider items-center pb-1">
                <SvgWrapper width={20} height={20}>
                    <FontAwesomeIcon icon='clock'/>
                </SvgWrapper>
                <div className="flex">{published.toString()}</div>
            </div>
            <Link to={link}>
                <h2 className="text-2xl md:text-3xl font-black">{title}</h2>
            </Link>
            {tags && (
                <div className="flex space-x-2 pt-3">
                    {tags.map(tag => <Tag tag={tag} key={tag}/>)}
                </div>
            )}
        </div>
    )
}

function PrevNext({type}: { type: 'prev' | 'next' }) {
    return (
        <button
            className="
      flex md:flex-1 border border-gray-200 rounded-lg justify-between p-4 shadow-card
      transform hover:scale-102 transition ease-in focus:outline-none
      "
        >
            <SvgWrapper width={24} height={24} className={type === 'prev' ? '-order-1' : 'order-5'}>
                <FontAwesomeIcon icon={type === 'prev' ? 'arrow-left' : 'arrow-right'}/>
            </SvgWrapper>
            <span className="order-1">{type === 'prev' ? 'Prev' : 'Next'}</span>
        </button>
    )
}

function PrevNextContainer() {
    return (
        <div className="flex flex-col md:flex-row justify-center p-10 space-y-4 md:space-y-0 md:space-x-6">
            <PrevNext type='prev'/>
            <PrevNext type='next'/>
        </div>
    )
}

export function Posts({posts}: { posts: PostData[] }) {
    return (
        <div className="px-4">
            <h2 className="text-3xl py-6">Latest posts</h2>
            <div className="flex flex-col space-y-16">
                {posts.map(({frontmatter: {slug, title, published, tags}}) => (
                    <PostSummary
                        key={slug}
                        title={title}
                        link={`/${slug}`}
                        published={published}
                        tags={tags}
                    />
                ))}
                <PrevNextContainer/>
            </div>
        </div>
    )
}

export default function PostIndex(props: Props) {
    const {
        data: {
            allMdx: {nodes},
        },
    } = props
    const posts = nodes.filter((node) => !!node.frontmatter.published)

    return (
        <div className="flex flex-col p-6">
            <Posts posts={posts}/>
        </div>
    )
}
