import * as React from "react"
import { graphql } from "gatsby"
import { PageData } from "page"
import { MDXRenderer } from "gatsby-plugin-mdx"

export const pageQuery = graphql`
  query staticPageBySlug($slug: String!) {
    mdx(frontmatter: { slug: { eq: $slug } }) {
      frontmatter {
        title
        slug
      }
      body
    }
  }
`
interface Props {
  data: { mdx: PageData }
}

export default function Page({ data: { mdx: post } }: Props) {
  const { frontmatter: { title }, body } = post
  return (
      <div className="flex flex-col p-6">
        <h1 className="text-4xl py-3 font-bold">{title}</h1>
        <div className="py-10 markdown">
          <MDXRenderer>{body}</MDXRenderer>
        </div>
      </div>
  )
}
