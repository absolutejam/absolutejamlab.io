export { createPosts } from "./post"
export { createStaticPages } from "./page"
export { createCheetsheets } from "./cheatsheet"
export { createIndices } from "./post-indices"
