import { PostData } from "post"
import { CreatePageProps } from "./shared"

interface PostsQueryResults {
  allMdx: {
    nodes: PostData[]
  }
}

export async function createPosts ({ createPage, graphql, reporter }: CreatePageProps) {
  const result = await graphql<PostsQueryResults>(`
    query postsQuery {
      allMdx(
        filter: {frontmatter: {kind: {eq: "Post"}}}
        sort: {fields: [frontmatter___published], order: DESC}
      ) {
        nodes {
          frontmatter {
            slug
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const template = require.resolve(`../templates/post`)
  result.data!.allMdx.nodes.forEach(({frontmatter: {slug}}) => {
    reporter.log(`Creating post: ${slug}`)
    createPage({
      path: slug,
      component: template,
      context: {slug},
    })
  })
}
