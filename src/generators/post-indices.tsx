import { PostData } from "post"
import { CreatePageProps } from "./shared"

interface PostIndicesQueryResults {
  // site: {
  //   siteMetadata: {
  //     blogEntriesPerPage: number
  //   }
  // }
  allMdx: {
    nodes: PostData[]
  }
}

// site {
//   siteMetadata {
//     blogEntriesPerPage
//   }
// }

export async function createIndices ({ createPage, graphql, reporter }: CreatePageProps) {
  const result = await graphql<PostIndicesQueryResults>(`
    query indicesQuery {
      allMdx(
        filter: {frontmatter: {kind: {eq: "Post"}}}
        sort: {fields: [frontmatter___published], order: DESC}
      ) {
        nodes {
          frontmatter {
            slug
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const template = require.resolve(`../templates/post-index`)
  const posts = result.data!.allMdx.nodes
  const postsPerPage = 10 // result.data!.site.siteMetadata.blogEntriesPerPage
  const numPages = Math.ceil(posts.length / postsPerPage)

  Array
      .from({ length: numPages })
      .forEach((_, i) => {
        createPage({
          path: i === 0 ? '/blog' : `/blog/${i + 1}`,
          component: template,
          context: {
            limit: postsPerPage,
            skip: i * postsPerPage,
            numPages,
            currentPage: i + 1,
          },
        })
      })
}
