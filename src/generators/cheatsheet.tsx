import { CheatsheetData } from "cheatsheet"
import { CreatePageProps } from "./shared"

interface CheatsheetQueryResults {
  allMdx: {
    nodes: CheatsheetData[]
  }
}

export async function createCheetsheets ({ createPage, graphql, reporter }: CreatePageProps) {
  const result = await graphql<CheatsheetQueryResults>(`
    query cheatsheetsQuery {
      allMdx(
        filter: {frontmatter: {kind: {eq: "Cheatsheet"}}}
      ) {
        nodes {
          frontmatter {
            name
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const template = require.resolve(`../templates/cheatsheet`)
  result.data!.allMdx.nodes.forEach(({frontmatter: {name}}) => {
    reporter.log(`Creating post: ${name}`)
    createPage({
      path: `cheatsheets/${name}`,
      component: template,
      context: {name},
    })
  })
}
