import {Actions, Reporter} from "gatsby";

export type CreatePageProps = {
    createPage: Actions['createPage'],
    graphql<TData, TVariables = any>(
        query: string,
        variables?: TVariables
    ): Promise<{
        errors?: any
        data?: TData
    }>,
    reporter: Reporter,
}
