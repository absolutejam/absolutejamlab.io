import { PageData } from "page"
import { CreatePageProps } from "./shared"

interface PageQueryResults {
  allMdx: {
    nodes: PageData[]
  }
}

export async function createStaticPages ({ createPage, graphql, reporter }: CreatePageProps) {
  const result = await graphql<PageQueryResults>(`
    query pagesQuery {
      allMdx(
        filter: {frontmatter: {kind: {eq: "Page"}}}
      ) {
        nodes {
          frontmatter {
            slug
          }
        }
      }
    }
  `)

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const template = require.resolve(`../templates/page`)
  result.data!.allMdx.nodes.forEach(({ frontmatter: { slug } }) => {
    reporter.log(`Creating page: ${slug}`)
    createPage({
      path: slug,
      component: template,
      context: { slug },
    })
  })
}
