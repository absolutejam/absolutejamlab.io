export interface CheatsheetFrontmatter {
  name: string
  title: string
}

export interface CheatsheetData {
  frontmatter: CheatsheetFrontmatter
  body: string
}
