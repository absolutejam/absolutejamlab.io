export interface PageFrontmatter {
  title: string
  slug: string
}

export interface PageData {
  frontmatter: PageFrontmatter
  body: string
}
