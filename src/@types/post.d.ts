export interface PostFrontmatter {
  title: string
  slug: string
  published: Date
  tags?: string[]
}

export interface PostData {
  frontmatter: PostFrontmatter
  body: string
}
