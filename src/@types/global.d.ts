declare module "@mdx-js/react" {
  export interface MDXProviderProps {
    children: React.ReactElement;
    components: { [k in string]: React.ReactElement | React.FunctionComponent<any> }
  }
  export const MDXProvider: (props: MDXProviderProps) => React.ReactElement
}

declare module "escape-string-regexp" {
  const escapeStringRegexp: (input: string) => string
  export = escapeStringRegexp
}

declare module "*.svg" {
  const svg: React.FunctionComponent<React.SVGAttributes<SVGElement>>
  export default svg
}

declare module "prism-react-renderer/prism" {
  const Prism: any // TODO:
  export default Prism
}
