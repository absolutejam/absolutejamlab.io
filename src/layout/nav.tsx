import * as React from "react"
import {Link} from "gatsby"
import {StaticImage} from "gatsby-plugin-image"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {SvgWrapper} from "../components/misc"

export type NavItemData = {
  icon: React.ReactElement,
  link: string,
  text: string,
}

const navItems: NavItemData[] = [
  {
    icon: <FontAwesomeIcon icon='comment' />,
    link: "/blog",
    text: "Blog",
  },
  {
    icon: <FontAwesomeIcon icon='book' />,
    link: "/cheatsheets",
    text: "Cheatsheets",
  },
  {
      icon: <FontAwesomeIcon icon='tag' />,
      link: "/tags",
      text: "Tags",
  },
  {
    icon: <FontAwesomeIcon icon='user' />,
    link: "/about",
    text: "About",
  },
]

const NavItem = ({ icon, link, text }: NavItemData) => {
  return (
      <Link key={link} to={link} data-tip={text}>
        <div className="flex group p-3 rounded-full fill-current hover:bg-gray-100">
        <SvgWrapper width={24} height={24}>
            {icon}
        </SvgWrapper>
        </div>
      </Link>
  )
}

export const Nav = () => {
  const left = (
      <Link to="/">
        <div className="md:flex hidden justify-start items-end text-gray-700 font-medium tracking-tight">
          <h1 className="flex text-2xl">absolutejam</h1>
          <span>.co.uk</span>
        </div>
      </Link>
  )

  const right = (
      <div className="flex md:space-x-4 flex-col md:flex-row flex-wrap flex-grow md:flex-none justify-center md:justify-end items-center">
        <div className="flex flex-wrap justify-center md:w-auto md:space-x-2">
          {navItems.map(NavItem)}
        </div>
        <div className="flex py-4 md:py-0 md:w-auto order-first md:order-last">
          <StaticImage
              src="../images/profile-pic.jpeg"
              alt="profile-pic"
              className="flex w-32 h-32 md:w-20 md:h-20 rounded-full border border-gray-200 shadow-xl"
          />
        </div>
      </div>
  )

  return (
      <nav className="flex w-full bg-gradient">
        <div className="flex mx-auto container flex-1 justify-between items-center px-6 md:px-12 py-4 md:py-8">
          {left}
          {right}
        </div>
      </nav>
  )
}
