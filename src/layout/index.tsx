import * as React from "react"
import {MDXProvider} from "@mdx-js/react"
import {Nav} from "./nav"
import {shortcodes} from "./shortcodes"

/** Initialise FontAwesome */
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'
library.add(fas)

/* Initialise ReactTooltip */
import ReactToolip from "react-tooltip"

export function Layout({children}: { children: React.ReactElement }) {
    return (
        <>
            <ReactToolip/>
            <Nav />
            <MDXProvider components={shortcodes}>
                <div
                    className="flex flex-col justify-center mx-auto md:pt-20 pb-20 container"
                    style={{maxWidth: 800}}
                >
                    {children}
                </div>
            </MDXProvider>
        </>
    )
}
