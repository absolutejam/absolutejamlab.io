import React from "react"
import Highlight, { defaultProps } from "prism-react-renderer"
import theme from 'prism-react-renderer/themes/github'

import Prism from "prism-react-renderer/prism"
const globalThis = (typeof global !== "undefined" ? global : window)
Object.assign(globalThis, { Prism }) // Avoids type-check errors

// Extra languages
require("prismjs/components/prism-typescript")
require("prismjs/components/prism-rust")
require("prismjs/components/prism-yaml")
require("prismjs/components/prism-fsharp")

export function CodeBlock({ children }: { children: React.ReactElement }) {
  const className = children.props.className || ''
  const matches = className.match(/language-(?<lang>.*)/)
  const lang = 
    matches && matches.groups && matches.groups.lang
    ? matches.groups.lang
    : ''
  const pre = children.props.children.trim()

  if (lang === "mermaid") {
    return children.props.children
  }
  else {
    return (
      <Highlight {...defaultProps} code={pre} language={lang} theme={theme}>
        {({ className, style, tokens, getLineProps, getTokenProps }) => (
          <pre className={className} style={style}>
            {tokens.map((line, i) => (
              <div key={i} {...getLineProps({ line, key: i })}>
                {line.map((token, key) => (
                  <span key={key} {...getTokenProps({ token, key })} />
                ))}
              </div>
            ))}
          </pre>
        )}
      </Highlight>
    )
  }
};
