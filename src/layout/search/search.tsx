import React, { useEffect, useState, createRef } from "react"
import { connectSearchBox, InstantSearch } from "react-instantsearch-dom"
import { SearchResults } from "./search-results"
import algoliasearch from "algoliasearch/lite"

/**
 * Component used to display the actual Html input box
 */
const SearchBox = connectSearchBox(({ refine, currentRefinement }) => {
  const searchRef = createRef<HTMLInputElement>()

  const searchHandler = (e: KeyboardEvent) => {
    if (e.key === "/") {
      e.preventDefault()
      if (searchRef.current) {
        searchRef.current?.focus()
      }
    }

    if (e.key === "Escape" && document.activeElement === searchRef.current) {
      searchRef.current?.blur()
      if (searchRef.current) {
        refine("")
      }
    }
  }

  const input = (
    <input
      className="input"
      type="text" 
      placeholder="Find stuff..."
      aria-label="Search"
      ref={searchRef}
      onChange={(e) => refine(e.target.value)}
      value={currentRefinement}
    />
  )

  useEffect(() => {
    document.addEventListener("keydown", searchHandler)
    return () => {
      document.removeEventListener("keydown", searchHandler)
    }
  }, [input])

  return input
})

export const Search = () => {
  const searchClient = algoliasearch(
    process.env.GATSBY_ALGOLIA_APP_ID as string,
    process.env.GATSBY_ALGOLIA_SEARCH_KEY as string
  )

  const indices = [{ name: "absolutejam-blog" }] //TODO: Config
  const [query, setQuery] = useState("")

  return (
    <div className="flex relative">
      <InstantSearch
        searchClient={searchClient}
        indexName={indices[0].name}
        onSearchStateChange={({ query }) => setQuery(query)}
      >
        <SearchBox />
        {query.length > 0 
          ? <SearchResults indices={indices} clearQuery={() => setQuery("")} />
          : undefined}
      </InstantSearch>
    </div>
  )
}
