import React from "react"
import { Link } from "gatsby"
import {
  connectStateResults,
  Highlight,
  Hits,
  Index,
  Snippet,
} from "react-instantsearch-dom"
import { IndexSpec } from "./types"

interface HitProps {
  slug: string
  title: string
  objectID: string
  _highlightResult: any
  __position: number
}

const HitCountDisplay = connectStateResults(({ searchResults }) => {
  const hitCount = searchResults && searchResults.nbHits
  const hitCountContent =
    hitCount > 0 
      ? `${hitCount} hit${hitCount !== 1 ? 's' : ''}`
      : "No hits"

  return (
    <div className="search-results-hit-count">
      {hitCountContent}
    </div>
  )
})

function PageHit ({ hit, clearQuery }: { hit: HitProps, clearQuery: () => void }) {
  const to = hit.slug === "/" ? "/" : `/${hit.slug}`
  return (
    <Link to={to} key={hit.slug} className="search-result-hit" onClick={clearQuery}>
      <h2>
        <Highlight attribute="title" hit={hit} tagName="mark" />
      </h2>
      <div className="search-result-info">
        <div className="search-excerpt">
          <Snippet attribute="excerpt" hit={hit} tagName="mark" />
        </div>
        <div className="search-slug">{hit.slug}</div>
      </div>
    </Link>
  )
}

export function SearchResults ({ indices, clearQuery }: { indices: IndexSpec[], clearQuery: () => void }) {
  // Partially-apply `clearQuery` into the `PageHit`
  const PageHitWithClearQuery = ({ hit }: { hit: HitProps }) => <PageHit hit={hit} clearQuery={clearQuery} />
  return (
    <div className="search-results">
      {indices.map((index) => (
        <Index key={index.name} indexName={index.name}>
          <HitCountDisplay />
          <Hits hitComponent={PageHitWithClearQuery} />
        </Index>
      ))}
    </div>
  )
}
