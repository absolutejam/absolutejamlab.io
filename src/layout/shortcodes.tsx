import {Note} from "../doc-components/Note"
import {CodeBlock} from "./prism-codeblock"

export const shortcodes = {
    Note,
    pre: CodeBlock,
}

