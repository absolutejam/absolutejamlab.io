import * as React from "react"

export function Note({ children }: { children: React.ReactElement[] }) {
  return (
    <div className="flex flex-1 flex-row flex-wrap space-y-4 md:space-y-0 space-y-1 border border-gray-200 rounded-lg items-center px-8 py-6 my-4 shadow-card transform hover:scale-102 transition ease-in">
      {children}
    </div>
  )
}
