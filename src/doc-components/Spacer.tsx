import React from "react";
import {SvgWrapper} from "../components";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export function Spacer() {
    return (
        <div className="flex py-10 justify-center items-center text-gray-200">
            <SvgWrapper width={32} height={32}>
                <FontAwesomeIcon icon='arrow-down' />
            </SvgWrapper>
        </div>
    )
}

