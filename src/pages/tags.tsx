import * as React from "react"
import { graphql, Link } from "gatsby"
import { PostData } from "post"

export const getPostsGroupedByTagQuery = graphql`
  query allPostsGroupedByTag {
    allMdx(
      sort: { fields: [frontmatter___published], order: DESC }
      filter: { frontmatter: { kind: { eq: "Post" } } }
    ) {
      group(field: frontmatter___tags) {
        fieldValue
        nodes {
          frontmatter {
            title
            slug
            published(formatString: "MMMM DD, YYYY")
            tags
          }
        }
      }
    }
  }
`

interface Props {
  data: {
    allMdx: {
      group: {
        fieldValue: string
        nodes: PostData[]
      }[]
    }
  }
}

export const TagSection = ({
  tagName,
  posts,
}: {
  tagName: string
  posts: PostData[]
}) => (
  <div className="flex flex-col space-y-1 p-8 border-gray-200 border rounded-lg transform hover:scale-102 transition ease-in focus:outline-none shadow-card">
    <div className="flex items-center justify-between border-b border-gray-200 pb-4">
      <h2 id={`tag-${tagName}`} className="flex text-2xl font-bold">
        &#35;{tagName}
      </h2>
      <span className="text-2xl">
        {posts.length.toString()} {posts.length > 1 ? "posts" : "post"}
      </span>
    </div>

    <div className="flex flex-col">
      <div className="flex flex-col space-y-2 pt-3">
        {posts.map(({ frontmatter: { title, published, slug } }) => (
          <div key={slug} className="flex flex-1 justify-between">
            <div className="flex">
              <Link to={`/${slug}`}>{title}</Link>
            </div>
            <div className="flex text-sm tracking-wide text-gray-400">
              {published.toString()}
            </div>
          </div>
        ))}
      </div>
    </div>
  </div>
)

const TagSummaries = ({ tagGroups }: { tagGroups: TagGroup[] }) => {
  return (
    <div className="flex flex-col p-6">
      <h1 className="text-4xl font-bold py-3">All tags</h1>
      <div className="flex flex-wrap space-x-2 py-4">
        {tagGroups.map(({ tagName }) => (
          <Link to={`#tag-${tagName}`}>
            <button
              key={`${tagName}`}
              className="flex rounded-lg text-gray-400 bg-gray-100 hover:bg-gray-200 px-3 py-2 tracking-wider focus:outline-none"
            >
              {tagName}
            </button>
          </Link>
        ))}
      </div>
      <div className="flex flex-col space-y-16 py-6">
        {tagGroups.map(({ tagName, posts }) => (
          <TagSection
            key={`${tagName}-section`}
            tagName={tagName}
            posts={posts}
          />
        ))}
      </div>
    </div>
  )
}

interface TagGroup {
  tagName: string
  posts: PostData[]
}

const TagsPage = ({
  data: {
    allMdx: { group: groups },
  },
}: Props) => {
  const tagGroups: TagGroup[] = groups.map(({ fieldValue, nodes }) => {
    return { tagName: fieldValue, posts: nodes }
  })
  return <TagSummaries tagGroups={tagGroups} />
}

export default TagsPage
