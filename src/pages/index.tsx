import * as React from "react"
import { graphql } from "gatsby"
import { PostData } from "post"
import { Intro } from "../components"
import { Spacer } from "../doc-components"
import { Posts } from "../templates/post-index";

export const query = graphql`
  query index {
    intro: mdx(
      frontmatter: { 
        kind: { eq: "Fragment"}
        name: { eq: "intro" }
      }
    ) {
      body
    }
    
    allMdx(
      sort: { fields: [frontmatter___published], order: DESC  }
      filter: { frontmatter: { kind: { eq: "Post" } } }
      limit: 6
    ) {
      nodes {
        frontmatter {
          slug
          title
          tags
          published(formatString: "MMMM DD, YYYY")
        }
      }
    }
  }
`
interface IndexQueryResults {
  data: {
    intro: { body: string }
    allMdx: {
      nodes: PostData[]
    }
  }
}

export default function Home(props: IndexQueryResults) {
  const {
    data: {
      intro: { body: introBody },
      allMdx: { nodes },
    },
  } = props

  const posts = nodes.filter((node) => !!node.frontmatter.published)

  return (
    <>
      <Intro introBody={introBody} />
      <Spacer />
      <Posts posts={posts} />
    </>
  )
}
