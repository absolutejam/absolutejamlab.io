import * as React from "react"
import {graphql, Link} from "gatsby"

export const getCheatsheets = graphql`
    query allCheatsheets {
        allMdx(
            filter: { frontmatter: { kind: { eq: "Cheatsheet" } } }
            sort: { fields: [frontmatter___slug], order: DESC }
        ) {
            nodes {
                frontmatter {
                    name
                    title
                }
            }
        }
    }
`

interface CheatsheetQueryResults {
    data: {
        allMdx: {
            nodes: {
                frontmatter: {
                    name: string
                    title: string
                }
            }[]
        }
    }
}

function CheatsheetLink({ title, name }: {  title: string, name: string }) {
    return (
        <div
            className="flex flex-col space-y-1 p-8 border-gray-200 border rounded-lg transform hover:scale-102 transition ease-in focus:outline-none shadow-card">
            <Link to={name}>
                <div className="flex items-center justify-between">
                    <h2 id={`tag-${name}`} className="flex text-2xl font-bold">
                        {title}
                    </h2>
                </div>
            </Link>
        </div>
    )
}

function CheatsheetCategories({ categories }: {  categories: { name: string; title: string }[] }) {
    return (
        <div className="flex flex-col p-6">
            <h1 className="text-4xl font-bold py-3">Cheatsheets</h1>
            <p className="py-3">
                A small collection of cheatsheets, that provide quick reference
                for a number of tools.
            </p>
            <div className="flex flex-col space-y-10 py-6">
                {categories.map(({name, title}) => (
                    <CheatsheetLink key={name} title={title} name={name}/>
                ))}
            </div>
        </div>
    )
}

export default function CheatsheetPage({ data: { allMdx: { nodes }}}: CheatsheetQueryResults) {
    return (
        <CheatsheetCategories
            categories={nodes.map(({frontmatter}) => frontmatter)}
        />
    )
}
