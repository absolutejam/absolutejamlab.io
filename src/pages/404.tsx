import * as React from "react"

export default function NotFoundPage() {
  return (
    <main className="flex flex-1 justify-center p-8 font-bold">
      <h1 className="text-3xl">Not found</h1>
    </main>
  )
}
