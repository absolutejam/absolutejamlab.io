# Absolutejam - Blog

## Developing

  - Set up node via. `nvm`

    ```
    nvm install 14
    nvm use 14
    ```

  - Spin up the dev server

    ```    
    yarn develop
    ```
