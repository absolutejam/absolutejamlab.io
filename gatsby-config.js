//@ts-nocheck
'use strict';

/**
 * This is a shim to allow us to write all of the gatsby config
 * in typescript (via. `ts-node`)
 */

/**
* Source-map-support mimics node's stack trace making debugging easier
* ts-node register helps importing and compiling TypeScript modules into JS
*/
require('source-map-support').install();
require('ts-node').register(
  // {
  //   compilerOptions: {
  //     target: 'esnext',
  //     module: 'esnext',
  //   },
  // }
);

module.exports = require('./config/gatsby-config.ts');
